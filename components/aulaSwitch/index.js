import React, {Component} from 'react';
import {View, Text, StyleSheet,Switch} from 'react-native';

class AulaSwitch extends Component{
    constructor(props){
        super(props);
        this.state={
            status:false
        }
    }
    render(){
        return(
            <View style={styles.areaSwitchs}>
                <Text style={styles.fontHeader}> exemplo de Switch</Text>
                <Switch
                thumbColor='red'
                value={this.state.status}
                onValueChange={(valorSwitch)=> this.setState({status:valorSwitch})}
                />
                <Text style={styles.fontPadrao}> valor:{(this.state.status ? "ativo":"inativo")}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    areaSwitchs:{
        height: 700,
        backgroundColor:'#eeeeee',
        alignSelf:'stretch',
        borderBottomWidth:2,
        paddingBottom:5,
        margin: 10,
    },
    fontHeader:{
        alignSelf:'center',
        fontWeight:'bold',
        fontSize:28
    },
    fontPadrao:{
        fontSize:25,
        alignSelf:'center'
    },
  });

export default AulaSwitch