import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Slider from '@react-native-community/slider';

class AulaSlider extends Component{
    constructor(props){
        super(props);
        this.state={
            valor:0
        }
    }
    render(){
        return(
            <View style={styles.areaSliders}>
                <Text style={styles.fontHeader}>Exemplos de Sliders</Text>
                <Slider 
                style={styles.s}
                minimumValue={0}
                maximumValue={100}
                minimumTrackTintColor ="#ff0000"
                onValueChange={(valorSelecionado)=> this.setState({valor:valorSelecionado})}
                />
                <Text style={styles.fontPadrao}>{this.state.valor.toFixed(1)}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    areaSliders:{
      height: 700,
      backgroundColor:'#eeeeee',
      alignSelf:'stretch',
      borderBottomWidth:2,
      paddingBottom:5,
      margin: 10,
      
    },
    fontHeader:{
        alignSelf:'center',
        fontWeight:'bold',
        fontSize:28
    },
    fontPadrao:{
        fontSize:25,
        alignSelf:'center'
    },
    styleSlider:{
        padding: 20,
        marginRight:10,
        marginLeft:10
    }
  });

  export default AulaSlider