import React, {Component} from 'react';
import {View, Text,Image,StyleSheet } from 'react-native';

export default class TelaInicial extends Component{
    render(){
        return(
            <View style={styles.base}>
                <Text style={styles.text}>Este é um App criado com as aulas do curso de React-Native</Text>
                <Text style={styles.text}>os exemplos a seguir sao atividades do curso</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    base:{
    
      height: 700,
      backgroundColor:'#eeeeee',
      justifyContent:'center',
      borderBottomWidth:2,
      paddingBottom:5,
      margin: 10,
      
    },
    text:{
      fontWeight:'bold',
      fontSize:20,
      textAlign:'center'
    },
  });