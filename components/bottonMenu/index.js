import React, {Component} from 'react';
import {View, Text,StyleSheet } from 'react-native';

class BottonMenu extends Component{
    render(){
      return(
        <View style={styles.footer}>
          <Text style={styles.fontPadrao}>este é um footer com algumas coisas escritas</Text>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    footer:{
      backgroundColor:'#444444',
      height: 40,
      borderTopWidth:2,
      
    },
    fontPadrao:{
      fontSize:15,
      alignSelf:'center'
  },
  });

  export default BottonMenu;