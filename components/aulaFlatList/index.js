import React, {Component} from 'react';
import {View, Text,StyleSheet,FlatList} from 'react-native';

class AulaFlatList extends Component{
    constructor(props){
        super(props)
        this.state = {
            feed:[
                {nome:'lucas',idade:24,user:'lucasbqf'},
                {nome:'roberto',idade:19,user:'roberto123'},
                {nome:'mario',idade:35,user:'mastergames'},
                {nome:'joão',idade:39,user:'jbarbosa'},
                {nome:'marcio',idade:25,user:'ManoHell'},
                {nome:'joilson',idade:25,user:'joinha'},
                {nome:'marinho',idade:25,user:'RBmar'},
                {nome:'getulio',idade:25,user:'Vargas'},

            ]
        };
    }
    render(){
      return(
        <View style={styles.area}>
            <Text style={styles.fontHeader}>FlatList</Text>
            <FlatList 
                style = {styles.flatList}
                horizontal= {true}
                data={this.state.feed}
                keyExtractor={(item) =>item.user}
                renderItem={({item}) => <Pessoa data={item}/>}
            />
            <FlatList 
                style = {styles.flatList}
                horizontal= {true}
                data={this.state.feed}
                keyExtractor={(item) =>item.user}
                renderItem={({item}) => <Pessoa data={item}/>}
            />
            <FlatList 
                style = {styles.flatList}
                horizontal= {true}
                data={this.state.feed}
                keyExtractor={(item) =>item.user}
                renderItem={({item}) => <Pessoa data={item}/>}
            />
        </View>
      );
    }
}


class Pessoa extends Component{
    render(){
        return(
            <View style={styles.pessoa}>
                <Text>Nome:{this.props.data.nome}</Text>
                <Text>Idade:{this.props.data.idade}</Text>
                <Text>User:{this.props.data.user}</Text>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    area:{
        height: 700,
        backgroundColor:'#eeeeee',
        alignSelf:'stretch',
        borderBottomWidth:2,
        paddingBottom:5,
        margin: 10,
        alignContent:'center',
        
      },
      fontHeader:{
          alignSelf:'center',
          fontWeight:'bold',
          fontSize:28
      },
      fontPadrao:{
          fontSize:25,
          alignSelf:'center'
      },
      pessoa:{
        margin: 5,
        height: 100,
        width: 150,
        backgroundColor:'#bbbbbb',
      },
      flatList:{
        backgroundColor:'#999999',
      }
  });

  export default AulaFlatList;