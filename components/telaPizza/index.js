import React, {Component} from 'react';
import {View, Text,Image,StyleSheet,TouchableOpacity } from 'react-native';
import {Picker} from '@react-native-picker/picker';

class TelaPizza extends Component{
    constructor(props){
        super(props);
        this.state={
            pizza:0,

            pizzas:[
                {key:1, nome: 'Strogonof',valor: 35},
                {key:2, nome: 'Calabresa',valor: 32},
                {key:3, nome: 'Brigadeiro',valor: 31},
                {key:4, nome: 'frango',valor: 30},
                {key:5, nome: 'banana',valor: 25},
            ]
        };
    };
    render(){
        let pizzaItem = this.state.pizzas.map((v,k)=>{
            return <Picker.Item key={k} value={k} label={v.nome}/>
        })
        return(
            <View style={styles.areaPizza}>
                <Text style={styles.textoMenu}>Menu Pizza</Text>

                <Picker 
                selectedValue={this.state.pizza}
                onValueChange={(itemValue, itemIndex)=> this.setState({pizza: itemValue})}
                >

                {pizzaItem}
                </Picker>

                <Text style={styles.fontPadrao}>Voce escolheu: {this.state.pizzas[this.state.pizza].nome}</Text>
                <Text style={styles.fontPadrao}>R$ {this.state.pizzas[this.state.pizza].valor.toFixed(2)}</Text>
                <Text> escolhido {this.state.pizza}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    areaPizza:{
      height: 700,
      backgroundColor:'#eeeeee',
      alignSelf:'stretch',
      borderBottomWidth:2,
      paddingBottom:5,
      margin: 10,
      
      
    },
    textoMenu:{
        alignSelf:'center',
        fontWeight:'bold',
        fontSize:28
    },
    fontPadrao:{
        fontSize:25,
        alignSelf:'center'
    }
  });


  export default TelaPizza;
