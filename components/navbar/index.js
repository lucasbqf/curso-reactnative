import React, {Component} from 'react';
import {View, Text,Image,StyleSheet,TouchableOpacity } from 'react-native';

class NavBar extends Component{
    render(){
      return(
        <View style={styles.topmenu}>
          <Text style={styles.textnavbar}>
            a Navbar vem aqui!
          </Text>
        </View>
      );
    }
}

const styles = StyleSheet.create({
    topmenu:{
      
      flexDirection: 'row',
      backgroundColor:'#888888',
      height: 40,
      alignSelf:'stretch',
      alignItems: 'center',
      borderBottomWidth:2,
      paddingBottom:5
      
    },
    textnavbar:{
      flex: 1,
      fontWeight:'bold',
      alignSelf:'center',
      textAlign:'center'
    },
  });

  export default NavBar;