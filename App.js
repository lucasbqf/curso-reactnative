import React, {Component} from 'react';
import {View,StyleSheet,ScrollView } from 'react-native';
import NavBar from './components/navbar'
import BotonMenu from './components/bottonMenu'
import TelaPizza from './components/telaPizza'
import TelaInicial from './components/telaInicial';
import  AulaSlider  from './components/aulaSlider';
import AulaSwitch from './components/aulaSwitch';
import AulaFlatList from './components/aulaFlatList';

class App extends Component{

  render(){
    return(
      <View style={styles.mainscreen} >
        <NavBar/>
        <ScrollView style={{flex:1}}>
          <TelaInicial style ={styles.conteiner}/>
          <TelaPizza style ={styles.conteiner}/>
          <AulaSlider style ={styles.conteiner}/>
          <AulaSwitch style ={styles.conteiner}/>
          <AulaFlatList style ={styles.conteiner}/>
        </ScrollView>
        

        <BotonMenu/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainscreen:{
    paddingTop: 30,
    paddingLeft: 2,
    paddingRight:2,
    flex: 1,
    backgroundColor:'#333333',
  },
  conteiner:{
    padding: 10,
    borderBottomColor:'#111111',
    borderBottomWidth: 2
  }
});

export default App;